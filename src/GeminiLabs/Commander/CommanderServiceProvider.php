<?php namespace GeminiLabs\Commander;

use Illuminate\Support\ServiceProvider;

class CommanderServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerCommandTranslator();
		$this->registerCommandBus();
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array(
			'commander',
		);
	}

	/**
	 * Register the command translator binding.
	 */
	protected function registerCommandTranslator()
	{
		$this->app->bind(
			'GeminiLabs\Commander\Contracts\CommandTranslatorInterface',
			'GeminiLabs\Commander\CommandTranslator'
		);
	}

	/**
	 * Register the command bus implementation.
	 */
	protected function registerCommandBus()
	{
		$this->app->bindShared( 'GeminiLabs\Commander\Contracts\CommandBusInterface',
			function () {
				return $this->app->make( 'GeminiLabs\Commander\CommandBus' );
			}
		);
	}
}
