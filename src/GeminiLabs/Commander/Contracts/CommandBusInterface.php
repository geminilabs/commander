<?php namespace GeminiLabs\Commander\Contracts;

interface CommandBusInterface
{
	/**
	 * Execute a command
	 *
	 * @param $command
	 *
	 * @return mixed
	 */
	public function execute( $command );
}
