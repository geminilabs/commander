<?php namespace GeminiLabs\Commander\Contracts;

interface CommandHandlerInterface
{
	/**
	 * Handle the command.
	 *
	 * @param $command
	 *
	 * @return mixed
	 */
	public function handle( $command );
}
