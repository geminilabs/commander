<?php namespace GeminiLabs\Commander\Contracts;

interface CommandTranslatorInterface
{
	/**
	 * Translate a command to its decorator counterpart
	 *
	 * @param $command
	 *
	 * @return mixed
	 */
	public function toCommandDecorator( $command );

	/**
	 * Translate a command to its handler counterpart
	 *
	 * @param $command
	 *
	 * @return mixed
	 * @throws /HandlerNotRegisteredException
	 */
	public function toCommandHandler( $command );
}
