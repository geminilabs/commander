<?php namespace GeminiLabs\Commander\Contracts;

interface DispatcherInterface
{
	/**
	 * Dispatch all raised events.
	 *
	 * @param array $events
	 */
	public function dispatch( array $events );
}
