<?php namespace GeminiLabs\Commander\Traits;

use GeminiLabs\Commander\Contracts\DispatcherInterface;
use App;

trait DispatchableTrait
{
	/**
	 * The Dispatcher instance.
	 *
	 * @var DispatcherInterface
	 */
	protected $dispatcher;

	/**
	 * Dispatch all events for an entity.
	 *
	 * @param object $entity
	 */
	public function dispatchEventsFor( $entity )
	{
		return $this->getDispatcher()->dispatch( $entity->releaseEvents() );
	}

	/**
	 * Set the dispatcher instance.
	 *
	 * @param mixed $dispatcher
	 */
	public function setDispatcher( DispatcherInterface $dispatcher )
	{
		$this->dispatcher = $dispatcher;
	}

	/**
	 * Get the event dispatcher.
	 *
	 * @return DispatcherInterface
	 */
	public function getDispatcher()
	{
		return $this->dispatcher ? : App::make( 'GeminiLabs\Commander\EventDispatcher' );
	}
}
