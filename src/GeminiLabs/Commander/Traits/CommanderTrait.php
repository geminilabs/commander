<?php namespace GeminiLabs\Commander\Traits;

use App;
use Input;
use InvalidArgumentException;
use ReflectionClass;

trait CommanderTrait
{
	/**
	 * Decorate and execute the command.
	 *
	 * @param  string $command
	 * @param  array  $input
	 *
	 * @return mixed
	 */
	protected function execute( $command, array $input = null )
	{
		$input = $input ? : Input::all();

		$command = $this->mapInputToCommand( $command, $input );

		$bus = $this->getCommandBus();

		return $bus->execute( $command );
	}

	/**
	 * Fetch the command bus
	 *
	 * @return mixed
	 */
	public function getCommandBus()
	{
		return App::make( 'GeminiLabs\Commander\CommandBus' );
	}

	/**
	 * Map an array of input to a command's properties.
	 *
	 * @param  string $command
	 * @param  array  $input
	 *
	 * @throws InvalidArgumentException
	 * @author Taylor Otwell
	 *
	 * @return mixed
	 */
	protected function mapInputToCommand( $command, array $input )
	{
		$dependencies = array();

		$class = new ReflectionClass( $command );

		foreach( $class->getConstructor()->getParameters() as $parameter ) {
			$name = $parameter->getName();

			if( array_key_exists( $name, $input ) ) {
				$dependencies[] = $input[ $name ];
			}
			elseif( $parameter->isDefaultValueAvailable() ) {
				$dependencies[] = $parameter->getDefaultValue();
			}
			else {
				throw new InvalidArgumentException( "Unable to map input to command: {$name}" );
			}
		}

		return $class->newInstanceArgs( $dependencies );
	}
}
