<?php namespace GeminiLabs\Commander;

use GeminiLabs\Commander\Contracts\CommandBusInterface;
use Illuminate\Foundation\Application;
use InvalidArgumentException;

class CommandBus implements CommandBusInterface
{
	/**
	 * @var Application
	 */
	protected $app;

	/**
	 * @var CommandTranslator
	 */
	protected $commandTranslator;

	/**
	 * @param Application       $app
	 * @param CommandTranslator $commandTranslator
	 */
	function __construct( Application $app, CommandTranslator $commandTranslator )
	{
		$this->app               = $app;
		$this->commandTranslator = $commandTranslator;
	}

	/**
	 * Execute the command
	 *
	 * @param object $command
	 *
	 * @return mixed
	 */
	public function execute( $command )
	{
		$this->executeDecorator( $command );

		$handler = $this->commandTranslator->toCommandHandler( $command );

		return $this->app->make( $handler )->handle( $command );
	}

	/**
	 * Execute the decorator
	 *
	 * @param object $command
	 *
	 * @return null
	 * @throws InvalidArgumentException
	 */
	public function executeDecorator( $command )
	{
		$decorator = $this->commandTranslator->toCommandDecorator( $command );

		if( $decorator ) {
			$instance = $this->app->make( $decorator );

			if( ! $instance instanceof CommandBusInterface ) {
				$message = 'The class to decorate must be an implementation of GeminiLabs\Commander\Contracts\CommandBusInterface';

				throw new InvalidArgumentException( $message );
			}

			$instance->execute( $command );
		}
	}
}
