<?php namespace GeminiLabs\Commander;

use GeminiLabs\Commander\Contracts\CommandTranslatorInterface;
use GeminiLabs\Commander\Exceptions\HandlerNotRegisteredException;

class CommandTranslator implements CommandTranslatorInterface
{
	/**
	 * Translate a command to its decorator counterpart
	 *
	 * @param $command
	 *
	 * @return mixed
	 */
	public function toCommandDecorator( $command )
	{
		$commandClass = get_class( $command );
		$decorator    = str_replace( 'Command', 'Decorator', $commandClass );

		if( ! class_exists( $decorator ) ) {

			return false;
		}

		return $decorator;
	}

	/**
	 * Translate a command to its handler counterpart
	 *
	 * @param $command
	 *
	 * @return mixed
	 * @throws /HandlerNotRegisteredException
	 */
	public function toCommandHandler( $command )
	{
		$commandClass = get_class( $command );
		$handler      = str_replace( 'Command', 'Handler', $commandClass );

		if( ! class_exists( $handler ) ) {
			$message = "Command handler [$handler] does not exist.";

			throw new HandlerNotRegisteredException( $message );
		}

		return $handler;
	}
}
